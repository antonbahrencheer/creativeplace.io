/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

window.events = new Vue();

window.flash = function (message) {
    window.events.$emit('flash', message);
}

// General components
Vue.component('befriend-button', require('./components/Friendship.vue'));
Vue.component('notifications', require('./components/Notification.vue'));
Vue.component('status', require('./components/UserStatus.vue'));
Vue.component('flash', require('./components/Flash.vue'));
Vue.component('edit-tags', require('./components/UserTags.vue'));
Vue.component('user-tags', require('./components/GetUserTags.vue'));
Vue.component('change-password', require('./components/passwordChange.vue'));
Vue.component('social-profiles', require('./components/socialProfiles.vue'));
Vue.component('friends', require('./components/Friends.vue'));

// User components
Vue.component('account-details', require('./components/AccountDetails.vue'));

// Session queue components
Vue.component('session-stats', require('./components/SessionStatistics.vue'));
Vue.component('session-configurator', require('./components/SessionConfigurator.vue'));
Vue.component('session-queue-screen', require('./components/SessionQueueScreen.vue'));

// import vueSmoothScroll from 'vue-smooth-scroll'
// Vue.use(vueSmoothScroll)

import VueScrollactive from 'vue-scrollactive'
Vue.use(VueScrollactive);

import VueAffix from 'vue-affix';
Vue.use(VueAffix);

const app = new Vue({
    el: '#app',
    data: {
        notifications: '',
        onlineUsers: '',
        panelState: false,
    },
    methods: {
        toggleFriendsPanel: function() {
            this.panelState = (this.panelState ? false : true);
            window.events.$emit('friendsPanelToggled', this.panelState);
        },

        tamperPanelToggle: function(tamperedState) {
            this.panelState = tamperedState;
        }
    },
    mounted: function () {
        window.events.$on('friendsPanelToggledBackchannel', this.tamperPanelToggle);
    },
    created() {
        axios.get('/notification/get').then(response => {
            this.notifications = response.data.notifications;
        });

        var userId = $('meta[name="user-id"]').attr('content');

        if (userId) {
            Echo.private('App.User.' + userId).notification((notification) => {
                this.notifications.push(notification);
            });
        }

        if (userId != '') {
            Echo.join('online')
                .here((users) => {
                    this.onlineUsers = users;
                })
                .joining((user) => {
                    this.onlineUsers.push(user);
                })
                .leaving((user) => {
                    this.onlineUsers = this.onlineUsers.filter((u) => {u != user});
                });
        }

    }
});

$(function(){
    $(".el-slider__button:eq(0)").css({
        'background-color' : '#41E1CD', 
        'border-color' : '#41E1CD'
    });
})