<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <!-- metas -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta id="token" name="csrf-token" content="{{ csrf_token() }}">
        <meta name="user-id" content="{{ Auth::check() ? Auth::user()->id : '' }}">
 
        <title>CreativePlace - Be creative together</title>

        {{-- <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"> --}}
        <link rel="icon" href="{{ asset('/favicon.png') }}" type="image/x-icon">

        <!-- pushing styles -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/foundation.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/landing.css') }}">
        @stack('styles')

        <!-- pushing scripts -->
        @stack('scripts')
    </head>
    <body>
        <div id="app">
            <section class="landing-section-one">
                <div class="landing-container">
                    <header>
                        <div class="logo"><a href="#"><img src="/resources/logo-landing.svg" alt=""></a></div>
                        <nav>
                            <ul>
                                <li><a href="#">About</a></li>
                                <li><a href="#">Community forum</a></li>
                                <li><a href="#">Donate</a></li>
                                <li><a href="/login">Sign in</a></li>
                            </ul>
                        </nav>
                    </header>
                    <h1>Be creative together</h1>
                    <div class="landing-button-wrap">
                        <a class="landing-button" href="/login">Sign up</a>
                        <a class="landing-button" href="#">Find creatives</a>
                    </div>
                </div>
                <img class="grafik-landing" src="/resources/grafik-landing-page.png">
            </section>
            <section class="landing-section-two">
                
            </section>
        </div>
        
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
