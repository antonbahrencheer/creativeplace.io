@extends('base')

@section('page')
@include('pages.header') 
    <div class="page-container login-page">


        <div class="panel panel-register">
            <div class="panel-header">Create new account</div>
            <hr class="panel-seperator"/>
                
                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <section class="panel-section no-last">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">
                                <span class="label-block">Full name<b>e.g. John Doe</b></span>

                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </label>
                        </div>                    

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">
                                <span class="label-block">Username<b>No spaces</b></span>

                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </label>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">
                                <span class="label-block">E-Mail Address<b>Valid e-mail address</b></span>

                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </label>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">
                                <span class="label-block">Password<b>6 characters or above</b></span>

                                <input id="password" type="password" class="form-control" name="password" style="margin-bottom: 5px;" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </label>
                        </div>
                    
                    </section>

                    
                    <section class="panel-section no-last">
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="login-btns">Sign up</button>
                            </div>
                        </div>
                    </section>
                </form>
            
        </div>





        <div class="panel login-panel">
            <div class="panel-header">Login</div>
            <hr class="panel-seperator"/>

            <form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <section class="panel-section no-last">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">
                            <span class="label-block">E-Mail Address</span>
                            <input id="login-email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </label>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">
                            <span class="label-block">Password</span>
                            <input id="login-password" type="password" class="form-control" name="password" style="margin-bottom: 5px;" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </label>
                    </div>
                </section>

                <section class="panel-section no-last">
                    <label style="cursor: pointer;">
                        <input style="width: 10px; height: 10px; float: left; margin: 4px 10px 0px 0px;" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                    </label>
                </section>

                <section class="panel-section no-last">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="login-btns">
                            Sign in
                        </button>

                        {{-- <a class="btn btn-link" href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a> --}}
                    </div>
                </section>
            </form>
        </div>
</div>
@include('pages.footer')
@endsection

@push('scripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/page.css') }}">
@endpush