<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <!-- metas -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta id="token" name="csrf-token" content="{{ csrf_token() }}">
        <meta name="user-id" content="{{ Auth::check() ? Auth::user()->id : '' }}">
 
        <title>CreativePlace - Be creative together</title>

        {{-- <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"> --}}
        <link rel="icon" href="{{ asset('/favicon.png') }}" type="image/x-icon">

        <!-- pushing styles -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/foundation.css') }}">
        <link rel="stylesheet" href="{{ asset('css/elementui.css') }}">
        <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
        @stack('styles')

        <!-- pushing scripts -->
        @stack('scripts')
    </head>
    <body>
        <div id="app">
            @yield('page')
            <flash message="{{ session('flash') }}"></flash>
        </div>
        
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.js"></script>
        {{--  <script src="{{ asset('js/elementui.js') }}"></script>  --}}
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
        
    </body>
</html>
