@extends('base')

@section('page')
    @include('pages.header')    

    <div class="page-container">

        <aside class="sidebar-queue">
            <div class="queue-info">
                <session-stats v-bind:onlineusers="onlineUsers"></session-stats>
            </div>

            
            <session-configurator></session-configurator>
            
        </aside>
        
        <main class="HolyGrail-content queue-content-section">
            <session-queue-screen></session-queue-screen>
        </main>
    </div>

    {{-- @include('pages.footer') --}}
@endsection

@push('scripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/page.css') }}">
@endpush