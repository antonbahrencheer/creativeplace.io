@extends('base')

@section('page')
    @include('pages.header')    

    <div class="HolyGrail-body">
        <main class="HolyGrail-content account-content-section">
                <form action="{{ route('account.update') }}" enctype="multipart/form-data" method="post">
                    {{ csrf_field() }} 
                    <input type="file" name="avatar" placeholder="avatar" value="{{ old('avatar ', $user->avatar)}}"><br/>
                    <input type="text" name="name" placeholder="name" value="{{ old('name', $user->name) }}"><br/>
                    <input type="text" name="location" placeholder="location" value="{{ old('location   ', $user->location)}}"><br/>
                    <textarea name="bio" placeholder="bio" value="{{ old('bio   ', $user->bio)}}"></textarea><br/>
                    <input type="text" name="username" placeholder="username" value="{{ old('username   ', $user->username)}}"><br/>
                    <input type="text" name="email" placeholder="email" value="{{ old('email    ', $user->email)}}"><br/>
                    <input type="text" name="link" placeholder="link" value="{{ old('link   ', $user->link)}}"><br/>
        
                    <input type="submit" name="senddatshit" value="Beam me up scawty">        
                </form>
        
                <form action="{{ route('account.tag.add') }}" method="post">
                    <input type="text" name="tag" placeholder="tag">
                    <input type="submit" name="add_tag" value="Add tag">
                </form>
        </main>
        <nav class="HolyGrail-left">
            <div class="floating-account-sidebar-user">
                <img src="{{ auth()->user()->getAvatar('avatar-small') }}">
                <div class="sidebar-part wrap">
                    <h1 class="is-12">{{ auth()->user()->name }}</h1>
                    <span class="is-12">3536 CP</span>
                </div>
            </div>

            <div class="floating-account-sidebar">
                <div class="sidebar-top">
                    Profile information
                </div>
                <ul>
                    <hr/>
                    <a href=""><li class="active">Profile information</li></a>
                    <a href=""><li>Password</li></a>
                    <a href=""><li>Social profiles</li></a>
                    <a href=""><li>Notification settings</li></a>
                    <a href=""><li>Billing</li></a>
                    <a href=""><li>Session log</li></a>
                </ul>
            </div>
        </nav>
        <aside class="HolyGrail-right">
            <div class="friend-sidebar" style="    margin: 0px 30px 0px 20px;">
                <div class="sidebar-top">
                    Friend list
                </div>
                <hr/>

                @forelse ($friends as $friend)
                    <div class="friend">
                        <img src="{{ $friend['avatar'] }}">
                        <span>
                            {{ $friend['name'] }} 
                            <status v-bind:friend="{{ $friend['id'] }}" v-bind:onlineusers="onlineUsers"></status>
                        </span>
                    </div>
                @empty
                    lol du har ingen venner din noob
                @endforelse
            </div>
        </aside>
    </div>
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {{-- @include('pages.footer') --}}
@endsection

@push('scripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/page.css') }}">
@endpush