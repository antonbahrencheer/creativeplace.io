@extends('base')

@section('page')
    @include('pages.header')    

    <div class="page-container">
        <aside class="sidebar-queue">
        </aside>
        
        <main class="HolyGrail-content queue-content-section">
        </main>
    </div>

    @include('pages.footer')
@endsection

@push('scripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/page.css') }}">
@endpush