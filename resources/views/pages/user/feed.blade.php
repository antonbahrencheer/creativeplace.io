@extends('base')

@section('page')
    {{-- @include('pages.header')     --}}

    <div class="HolyGrail-body">
        feed
    </div>
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {{-- @include('pages.footer') --}}
@endsection

@push('scripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/page.css') }}">
@endpush