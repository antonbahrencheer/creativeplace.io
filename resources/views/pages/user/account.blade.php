@extends('base')

@section('page')
    @include('pages.header')    
    <div class="page-container account-page">
        <main id="account-content" class="HolyGrail-content account-content-section">
            <account-details avatar="{{ auth()->user()->getAvatar('avatar-large') }}" route="{{ route('account.update') }}" csrf='{{ csrf_field() }}'></account-details>
        </main>
    </div>
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @include('pages.footer')
@endsection

@push('scripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/page.css') }}">
@endpush