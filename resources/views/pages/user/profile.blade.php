@extends('base')

@section('page')
    @include('pages.header')    
    <div class="page-container-full">
        <div class="panel floating-sidebar">
            <section class="panel-section no-last">
                <div class="profile-avatar-wrapper">
                    <img class="profile-avatar" src="{{ $avatar['large'] or $avatar['placeholder'] }}">
                </div>
                <div class="profile-info-wrapper">
                    <h3 class="profile-name">{{ $user['name'] }}</h3>
                    <h4 class="profile-location">{{ $user['location'] }}</h4>
                    <div class="networking-buttons">
                        <div class="follow"><button>Follow</button></div>
                        <befriend-button reciver="{{ $user['id'] }}" :compressed="false"></befriend-button>
                    </div>
                </div>
                <div class="pill-line">
                    <span class="rating-pill">CP {{ $rating }}</span>
                </div>
                <p class="bio">{{ $user['bio'] }}</p>
            </section>

            @if (count($tags) > 0)
                <section class="panel-section no-last">
                    <h3 class="panel-header">Creative tags</h3>
                    <div class="tag-container-pub">
                        @foreach ($tags as $tag)
                            <div class="user-tag-pub">{{ $tag }}</div>
                        @endforeach
                    </div>
                </section>
            @endif

            @if (count($socials) > 0)
                <section class="panel-section no-last">
                    <h3 class="panel-header">Socials</h3>
                    @foreach ($socials as $social => $value)                    
                        <div class="user-socials-pub">
                            <a href="https://dribbble.com/{{ $value }}">
                                <div class="user-social" style="mask-image: url(/resources/socials/{{ $social }}.svg); background: #444444;"></div>
                            </a>
                        </div>
                    @endforeach
                </section>
            @endif
        </div>
    </div>

    @include('pages.footer')    
@endsection

@push('scripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/page.css') }}">
@endpush