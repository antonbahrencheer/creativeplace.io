<header class="main-header">
    <ul class="main-menu">
        <a href="/" class="logo"><img src="{{ asset('storage/resources/logo.png') }}"></a>
        <li><a href="/find-session">Find session</a></li>
        <li><a href="/forum/battles">Battles</a></li>
        <li><a href="/forum">Community forum</a></li>
        <li><a href="/donate">Donate</a></li>
        <li><a href="/support">Support</a></li>
    </ul>

    @guest
        <ul class="user-login-wrap">
            <li><a href="/login">Sign up / Sign in</a></li>
        </ul>
    @endguest

    @auth        
        <div class="user-control">
            <li class="search"></li>
            <li class="friends-toggle" @click="toggleFriendsPanel"></li>
            <li class="messages"></li>
            <div class="placeholder-notifications"><notifications v-bind:notifications="notifications"></notifications></div>
            <li class="account-ico">
                <a href="/{{ auth()->user()->username }}"><img class="header-avatar" src="{{ auth()->user()->getAvatar('avatar-small') }}"></a>
                <ul class="dropdown-top">
                    <a href="/{{ auth()->user()->username }}"><li>Account</li></a>
                    <a href="/account"><li>Portfolio</li></a>   
                    <a href="/account"><li>Statistics</li></a>
                    <a href="/account"><li>Account details</li></a>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><li>Log out</li></a><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                </ul>
            </li>
        </div>
    @endauth
</header>

@auth        
<friends :friends="{{ $friends }}" :online="onlineUsers" :state="panelState"></friends>
@endauth