<?php

namespace App\Http\Controllers\Session;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Cache;

class SessionController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        return view('pages.session.queue', [
            'user' => $user,
            'friends' => json_encode($user->getFriendsOfUser($user)),
        ]);
    }    

    public function pingQueue(Request $request) 
    {
        // set vars
        $user = Auth::user();
        $test = '';

        // check if user has initiated his queue status
        $initiated = DB::table('session_queue')->select('id', 'potentials')->where('user_id', $user->id)->first();

        $analytics = DB::table('session_queue')->count();
        
        if($initiated) {
            // disconnect timed users somehow
            // evaluate users again and 
        }else {
            // enter current user into queue by calling init function
            $this->initQueue($user, $request);
        }
        
        // send back response
        return response()->json([
            'user' => $user->id,
            'data' => $request->time,
            'queueData' => $analytics,
            'potentials' => $initiated,
        ]);
    }

    public function initQueue($user, $data) 
    {
        // inserts reverse indexed durations into temp queue table
        $durationIds = [];
        for ($order = $data->time[0]; $order < ($data->time[1] + 1); $order++) { 
            $id = DB::table('session_durations')->select('id')->where('time_order', $order)->first()->id;
            DB::table('queue_user_durations')->insert([
                'user_id' => $user->id,
                'duration_id' => $id
            ]);  
            array_push($durationIds, $id);
        }

        // inserts reverse indexed categories into temp queue table
        $categoryIds = [];
        foreach ($data->categories as $category) {
            $id = DB::table('creative_categories')->select('id')->where('name', $category)->first()->id;
            DB::table('queue_user_categories')->insert([
                'user_id' => $user->id,
                'category_id' => $id
            ]);  
            array_push($categoryIds, (int)$id);
        }

        // gets auth user's rating
        $rating = $user->getRating($user);

        $queueInitiation = DB::table('session_queue')->insert([
            'user_id' => $user->id, 
            'potentials' => json_encode($this->evaluateMatches($rating, $categoryIds, $durationIds))
        ]);  
        
        return ($queueInitiation ? true : false);        
    }

    public function evaluateMatches($rating, $categories, $durations) 
    {
        $potentialDurationUsers = [];
        
        // gets users with overlapping durations
        $users = DB::table('queue_user_durations')
            ->select('user_id')
            ->whereIn('duration_id', $durations)
            ->distinct()
            ->get();
        
        foreach ($users as $user) {
            // adds users to array of potential partial matches
            if((int)$user->user_id && !in_array((int)$user->user_id, $potentialDurationUsers) && (int)$user->user_id != Auth::user()->id) {
                array_push($potentialDurationUsers, (int)$user->user_id);
            }
        }

        //if any of the durations overlapped proceed to find the id's which match overlapping categories
        if(count($potentialDurationUsers) > 0) {          
            $potentialCategoryUsers = [];

            //gets all users that matches a user id from the potential durations and a category from the category options
            $users = DB::table('queue_user_categories')
                ->select('user_id')
                ->whereIn('category_id', $categories)
                ->distinct()
                ->get();
            
            foreach ($users as $user) {
                // adds users to array of potential partial matches
                if((int)$user->user_id && !in_array((int)$user->user_id, $potentialCategoryUsers) && (int)$user->user_id != Auth::user()->id) {
                    array_push($potentialCategoryUsers, (int)$user->user_id);
                }
            } 

            //gets the intersecting potential matches that has both matching category and duration
            $potentialMatches = array_intersect($potentialCategoryUsers, $potentialDurationUsers);

            $matchEvaluations = [];

            // evaluates chance of being a good match on every potential candidate
            foreach ($potentialMatches as $user) {
                $match = User::find($user);

                // gets rating of the user being evaluated
                $matchRating = $match->getRating($match);
                
                // finds relative percentage of the current user's rating 
                $difference = (($matchRating/$rating)*100);

                $matchEvaluations[(int)$user] = (float)$difference;                
            }

            if(count($matchEvaluations) > 0) {
                return $matchEvaluations;
                // on update and not init ;==)))
                // DB::table('session_queue')
                //     ->where('user_id', Auth::user()->id)
                //     ->update(['potentials' => json_encode($matchEvaluations)]);
            }

        }else {
            return false;
        }

        return array_intersect($potentialCategoryUsers, $potentialDurationUsers);
    }

    public function leaveQueue() {
        $user = Auth::user();

        // removes the user from db queue tables 
        DB::table('session_queue')->where('user_id', '=', $user->id)->delete();
        DB::table('queue_user_durations')->where('user_id', '=', $user->id)->delete();
        DB::table('queue_user_categories')->where('user_id', '=', $user->id)->delete();

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function randomizeUserArray($count, $max) 
    {
        $users = [];
        $count = ($max < $count ? $max : $count);

        $key_count = 1;

        while (count($users) < $count) {
            $random_number = rand(1, $max);
            
            if(!array_key_exists($random_number, $users)) {
                $user = User::find($random_number);

                $users[$random_number]['path'] = $user->getAvatar('avatar-small');
                $users[$random_number]['username'] = $user->username;
            
                $key_count++;
            }
        }

        $users = array_chunk($users, 7);
        // dd($users);
        return $users;
    }

    public function getRandomAvatars() 
    {
        if(Cache::get('displayAvatars')) {
            $random_avatars = Cache::get('displayAvatars');
        }else {
            $expiresAt = now()->addMinutes(720); // expires every 12 hours
            $max_id = DB::table('users')->max('id');
            $random_avatars = $this->randomizeUserArray(21, $max_id);

            Cache::put('displayAvatars', $random_avatars, $expiresAt);
        }

        return response()->json($random_avatars);
    }

    public function getQueuePop($user_id = 'fjern når det laves rigtigt') 
    {
        $user = User::find(7);

        return response()->json([
            'avatar' => $user->getAvatar('avatar-large'),
            'username' => $user->username,
            'name' => $user->name,
            'location' => $user->location
        ]);
    }

    public function getCreativeCategories() 
    {
        $categories = DB::table('creative_categories')->orderBy('order')->where('status', 1)->pluck('name');
        return response()->json($categories);
    }

    function secondsToTime($inputSeconds) 
    {
        $secondsInAMinute = 60;
        $secondsInAnHour  = 60 * $secondsInAMinute;
        $secondsInADay    = 24 * $secondsInAnHour;
    
        // extract hours
        $hourSeconds = $inputSeconds % $secondsInADay;
        $hours = floor($hourSeconds / $secondsInAnHour);
    
        // extract minutes
        $minuteSeconds = $hourSeconds % $secondsInAnHour;
        $minutes = floor($minuteSeconds / $secondsInAMinute);
    
        // return the final format
        $response = '';
        
        if((int)$hours > 0) 
            $response .= (int)$hours . 'h';

        if((int)$hours > 0 && (int)$minutes > 0)
            $response .= ' ';

        if((int)$minutes > 0)
            $response .= (int)$minutes . 'm';

        return $response;
    }

    public function getDurations() 
    {
        $durations = DB::table('session_durations')->select('time_order', 'correlation')->orderBy('time_order')->get();
        $ranges = [];
        foreach($durations as $duration) {
            $ranges[$duration->time_order] = $this->secondsToTime($duration->correlation);
        }
        return response()->json($ranges);
    }
}
