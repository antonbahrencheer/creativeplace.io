<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Events\FriendshipRequested;
use App\Notifications\NotifyUser;

class FriendshipController extends Controller
{
    public function isFriend(Request $request)
    {
        $sender = Auth::user(); 
        if(!$sender || $sender->id == request('id')) {
            return response()->json([
                'identifier' => 'not_logged',
            ]);
        }else {
            $reciever = User::find(request('id'));

            $friendship = $sender->isFriendWith($reciever);

            return response()->json([
                'identifier' => 'friends',
                'text' => 'Unfriend',
                'status' => $friendship
            ]);
        }
    }

    public function requestFriendship(Request $request)
    {        
        $reciever = User::find(request('id'));
        $sender = Auth::user();

        $friendship = $sender->befriend($reciever);
        
        if($friendship) {
            // return($friendship);
            // FriendshipRequested::dispatch($friendship);
            $reciever->notify(new NotifyUser($friendship->sender_id));
        }

        return response()->json([
            'identifier' => 'requested',
            'text' => 'Friend request sent',
            'status' => $friendship
        ]);
    }

    public function revokeFriendship(Request $request)
    {        
        $reciever = User::find(request('id'));
        $sender = Auth::user();

        $friendship = $sender->unfriend($reciever);

        return response()->json([
            'identifier' => 'unfriend',
            'text' => 'Friend removed',
            'status' => $friendship
        ]);
    }

    public function acceptFriend(Request $request)
    {        
        $reciever = User::find(request('id'));
        $sender = Auth::user();
        
        $friendship = $sender->acceptFriendRequest($reciever);

        return response()->json([
            'identifier' => 'send',
            'text' => ucfirst(explode(' ',trim($reciever->name))[0]) . ' has been added',
            'status' => $friendship
        ]);
    }

    public function denyFriend(Request $request)
    {        
        $reciever = User::find(request('id'));
        $sender = Auth::user();
        
        $friendship = $sender->denyFriendRequest($reciever);

        return response()->json([
            'identifier' => 'deny',
            'text' => 'Request has been denied',
            'status' => $friendship
        ]);
    }

    public function isRequestPending(Request $request)
    {        
        $reciever = User::find(request('id'));
        $sender = Auth::user();
        
        $request_to = $sender->hasSentFriendRequestTo($reciever);

        if(!$request_to) {
            $request_from = $sender->hasFriendRequestFrom($reciever);

            if($request_from) {
                return response()->json([
                    'identifier' => 'from',
                    'status' => $request_from,
                    'text' => 'Accept friend request'
                ]);
            }else {
                return response()->json([
                    'identifier' => 'clean',
                    'status' => false,
                    'text' => 'Add Friend'
                ]);
            }
        }else {
            return response()->json([
                'identifier' => 'to',
                'status' => $request_to,
                'text' => 'Friend request pending'
            ]);
        }
    }
}
