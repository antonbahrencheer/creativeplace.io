<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User;

class AccountController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        $tags = $user->getTags($user);

        return view('pages.user.account', [
            'user' => $user,
            'tags' => $tags,
            'friends' => json_encode($user->getFriendsOfUser($user))
        ]);
    }

    public function getUserData()
    {
        $user = Auth::user();
        $user_data = [
            'name' => $user->name,
            'location' => $user->location,
            'username' => $user->username,
            'bio' => $user->bio,
            'email' => $user->email,
            'link' => $user->link,
            'rating' => $user->getRating($user)
        ];

        return response()->json($user_data);
    }

    public function update(Request $request)
    {
        $user = Auth::user();
                
        $validatedData = $request->validate([
            'avatar' => 'mimes:jpeg,bmp,png',
            'name' => 'required',
            'username' => 'required|unique:users,username,'.$user->id,
            'email' => 'required|email',
            'bio' => 'sometimes|required|string|between:0,140',
            'location' => 'sometimes|required|string',
            'link' => 'sometimes|required|url',
        ]);
        
        $response = false;

        if($validatedData) {
            $response = $user->update($validatedData);
        }else {
            return response()->json($validatedData);
        }

        if($request->avatar) {
            $user->clearMediaCollection('user.avatar')
                 ->addMediaFromRequest('avatar')
                 ->toMediaCollection('user.avatar', 'avatar');
        }        

        return response()->json($response);
    }

    public function getSocials(Request $request) {
        $user = User::find($request->user_id);
        return $user->socials;
    }

    public function updateSocials(Request $request) {
        $socials = [];
        foreach ($request->request as $social => $value) {
            if($value != null && $value != '') {
                $socials[$social] = $value;
            }
        }

        $user = Auth::user();
        $user->socials = (count($socials) > 0 ? json_encode($socials) : null);
        $user->save();

        return response()->json([
            'status' => 'success',
            'message' => 'Your social profiles has successfully been updated.'
        ]);
    }

    public function changePassword(Request $request) {
        // if all the variables are set 
        if($request->confirm && $request->new && $request->current) {
            // if the two new passwords match
            if($request->confirm === $request->new) {
                // gets new hash data from authed user (because of async data could change but old hash is cached) and checks old hash agains the plain string from request
                $user = User::find(Auth::user()->id);
                if(Hash::check($request->current, $user->password)) {
                    $user->password = bcrypt($request->new);
                    $user->save();
                    
                    $response = [
                        'status' => true,
                        'message' => 'Your password has successfully been changed!'
                    ];
                }else {
                    $response = [
                        'status' => false,
                        'message' => 'The current password didnt match, please try again.'
                    ];
                }
            }else {
                $response = [
                    'status' => false,
                    'message' => 'The new password doesn\'t match the comfirmation'
                ];
            }
        }else {
            $response = [
                'status' => false,
                'message' => 'Error, please check the password form carefully and try again.'
            ];
        }

        return response()->json($response);
    }

    public function updateAvatar(Request $request) {
        dd($request);
    }

    public function sortTags(Request $request) {
        $user = Auth::user();
        
        // updates all tags from new array to its position in array (key) 
        foreach($request->tags as $new_sort => $tag) {
            DB::table('user_tags')
                ->join('user_to_user_tag', 'user_tags.id', '=', 'user_to_user_tag.tag_id')
                ->where([
                    ['user_to_user_tag.user_id', '=', $user->id],
                    ['user_tags.name', '=', $tag['name']]
                ])
                ->update(['user_to_user_tag.sort' => $new_sort]);
        }

        return response()->json(true);
    }

    public function addTag(Request $request)
    {
        $user = Auth::user();
        $tag = ($request->tag ? $request->tag : false);
        $tag_count = DB::table('user_to_user_tag')->where('user_id', $user->id)->count();
        $sort = 0;

        // if auth has less than 10 tags and tag isset
        if($tag_count < 10 && $tag) {
            // does the desired tag already have an existing master
            $exists = DB::table('user_tags')->select('id')->where('name', $tag)->first();
            
            // if the tag already exists and if there's an existing connection between auth and master -> error
            if($exists) {
                $result = DB::table('user_tags')
                    ->join('user_to_user_tag', 'user_tags.id', '=', 'user_to_user_tag.tag_id')
                    ->select('user_tags.name')
                    ->where([
                        ['user_to_user_tag.user_id', '=', $user->id],
                        ['user_tags.id', '=', $exists->id]
                    ])
                    ->first();

                    if($result) {
                        return response()->json([
                            'status' => false,
                            'message' => 'Connection already exists'
                        ]);        
                    }
            }

            $tag_id = ($exists ? $exists->id : DB::table('user_tags')->insertGetId(['name' => $tag]));  

            $sort = (DB::table('user_to_user_tag')->select( DB::raw("MAX(sort) as max_sort "))->where('user_id', '=', $user->id)->first()->max_sort) + 1;
            
            // created connection between tag master and auth
            $inserted = DB::table('user_to_user_tag')->insert([
                'user_id' => $user->id,
                'tag_id' => $tag_id,
                'sort' => $sort
            ]);        
        }else {
            $msg = ($tag ? 'You already have 10 tags, delete a tag to add a new one' : 'Please write a valid tag name');

            return response()->json([
                'status' => false,
                'message' => $msg,
            ]);
        }
        
        return response()->json([
            'status' => $inserted,
            'message' => false,
            'sort' => $sort,
        ]);
    }

    public function removeTag(Request $request) 
    {
        $user = Auth::user();
        $tag_info = DB::table('user_tags')->where('name', $request->tag)->first();

        // removes tag connection
        DB::table('user_to_user_tag')
            ->where([
                ['user_id', '=', $user->id],
                ['tag_id', '=', $tag_info->id]
            ])
            ->delete();
        
        // counts remaining tag connections after deletion
        $tag_count = DB::table('user_to_user_tag')->where('tag_id', $tag_info->id)->count();
        
        // if there aren't any tag connections left for the given tag, delete the tag's reference point
        if($tag_count == 0) {
            DB::table('user_tags')->where('id', $tag_info->id)->delete();
        }

        return response()->json(true);
    }

    public function getTags()
    {
        $user = Auth::user();

        // gets all tags associated with authed user
        $result = DB::table('user_tags')
            ->join('user_to_user_tag', 'user_tags.id', '=', 'user_to_user_tag.tag_id')
            ->select('user_tags.name', 'user_to_user_tag.sort')
            ->where('user_to_user_tag.user_id', '=', $user->id)
            ->orderBy('user_to_user_tag.sort', 'asc')
            ->get();

        // edits output for desired end format
        $tags = [];
        foreach($result as $k => $tag) {
            array_push($tags, $tag->name);
        }

//        dd(response()->json($result));
        
        
        return response()->json($result->toArray());
    }

    public function filterTags(Request $request) 
    {
        // gets results grouped on destinct tag names and counts connections to each tag
        $result = DB::table('user_tags')->select( DB::raw("user_tags.name, COUNT(user_to_user_tag.tag_id) as tag_count "))
            ->join('user_to_user_tag', 'user_to_user_tag.tag_id', '=', 'user_tags.id')
            ->where('user_tags.name', 'like', $request->term . '%')
            ->groupBy('user_tags.name')
            ->limit(5)
            ->get();

        return response()->json($result);
    }
    
}