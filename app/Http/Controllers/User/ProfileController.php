<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        return view('pages.user.profile', [
            'user' => $user,
            'tags' => $user->getUserTags($user),
            'avatar' => $user->getAvatar(['large' => 'avatar-large', 'small' => 'avatar-small']),
            'rating' => $user->getRating($user),
            'socials' => json_decode($user->getSocials($user)),
            'friends' => json_encode($user->getFriendsOfUser($user)),
        ]);
    }
}
