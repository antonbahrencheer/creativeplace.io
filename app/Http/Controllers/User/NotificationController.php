<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notification;
use Illuminate\Support\Facades\Auth;
use App\User;

class NotificationController extends Controller
{
    public function getNotifications() {
        if (Auth::check()) {
            $notifications = Auth::user()->unreadNotifications;
            
            if($notifications != '[]') {
                
                foreach($notifications as $notification) {
                    if($notification->data['type'] == 'friend_request') {
                        $user = User::find($notification->data['id']);
                        $avatar = $user->getAvatar('avatar-small');
                        $name = $user->name;
                    }
                    
                    $response[$notification->id] = [
                        'type' => $notification->data['type'],
                        'notification' => $notification->id,
                        'data' => [
                            'id' => $notification->data['id'],
                            'name' => $name,
                            'avatar' => $avatar
                        ],
                    ];
                }
            }else {
                $response = false;
            }
        }else {
            $response = false;
        }

        return response()->json([
            'notifications' => $response
        ]);
    }

    public function readNotification(Request $request) {
        
        $response = Auth::user()->unreadNotifications()->find($request->id)->markAsRead();
        return response()->json([
            'data' => $response
        ]);
    }
}
