<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Hootlex\Friendships\Traits\Friendable;
use Hootlex\Friendships\Models\Friendship;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable implements HasMedia, HasMediaConversions
{
    use Notifiable;
    use HasMediaTrait;
    use Friendable;

    public $registerMediaConversionsUsingModelInstance = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'bio', 'socials', 'location', 'link', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getRouteKeyName()
    {
        return 'username';
    }

    public function getAvatar($size) {
        if(is_array($size)) {
            $avatars = Array();
            if($this->getFirstMedia('user.avatar')) {
                foreach($size as $key => $value) {
                    $avatars[$key] = $this->getFirstMedia('user.avatar')->getUrl($value);
                }
            }else{
                $avatars['placeholder'] = config('app.placeholder.avatar');
            }
            return $avatars;
        }else {
            if($this->getFirstMedia('user.avatar')) {
                $avatar = $this->getFirstMedia('user.avatar')->getUrl($size);
            }else{
                $avatar = config('app.placeholder.avatar');
            }
            return $avatar;
        }
    }

    public function getRating($user) {
        $sum = DB::table('rating_transactions')->where('user_id', $user->id)->sum('rating_transaction');
        return $sum;
    }

    public function getSocials($user) {
        return $user->socials;
    }

    public function getFriendsOfUser($user) {
        $friendships = $user->getAllFriendships();

        $friends = array();
        
        foreach($friendships as $friendship) {
            if($friendship->status == 1) {
                $friend_id = $user->id == $friendship->recipient_id ? $friendship->sender_id : $friendship->recipient_id;                
                $friend = $this->find($friend_id);
                
                $friends[$friendship->id] = [
                    'id' => $friend_id,
                    'name' => $friend->name,
                    'avatar' => $friend->getAvatar('avatar-large'),
                    'user' => $friend
                ];
            }
        }

        return $friends;
    }

    public function getUserTags($user)
    {
        // gets all tags associated with authed user
        $result = DB::table('user_tags')
            ->join('user_to_user_tag', 'user_tags.id', '=', 'user_to_user_tag.tag_id')
            ->select('user_tags.name')
            ->where('user_to_user_tag.user_id', '=', $user->id)
            ->get();

        // edits output for desired end format
        $tags = [];
        foreach($result as $k => $tag) {
            array_push($tags, $tag->name);
        }
       
        return $tags;
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('avatar-small')
            ->fit('crop', 46, 46)
            ->performOnCollections('user.avatar');

        $this->addMediaConversion('avatar-large')
            ->fit('crop', 130, 130)
            ->performOnCollections('user.avatar');
    }

    public function getTags($user) {
        
        

        return $user->id;
    }
}
