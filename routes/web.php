<?php

use App\Events\FriendshipRequested;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('auth.login');
});

Route::get('/register', function () {
    return view('auth.register');
});

Route::get('/', function () {
    if (Auth::check()) {
        return view('pages.user.feed');
    } else {
        return view('guests.landing');
    }
});

Route::middleware(['auth'])->group(function () {
    Route::get('/account', 'User\AccountController@index')->name('account');
    Route::post('/account', 'User\AccountController@update')->name('account.update');

    Route::post('/account/update', 'User\AccountController@update')->name('account.update');
    Route::post('/account/change/password', 'User\AccountController@changePassword')->name('account.change.password');
    Route::post('/account/update/socials', 'User\AccountController@updateSocials')->name('account.socials.update');
    Route::post('/account/socials/get', 'User\AccountController@getSocials')->name('account.socials.get');


    Route::post('/account/tag/add', 'User\AccountController@addTag')->name('account.tag.add');
    Route::post('/account/tag/remove', 'User\AccountController@removeTag')->name('account.tag.remove');
    Route::post('/account/tag/sort', 'User\AccountController@sortTags')->name('account.tags.sort');
    Route::post('/account/tags/filter', 'User\AccountController@filterTags')->name('account.tags.filter');
    Route::get('/account/tags/get', 'User\AccountController@getTags')->name('account.tags.get');
    Route::get('/account/user/data', 'User\AccountController@getUserData')->name('account.user.get');
    
    Route::get('/find-session', 'Session\SessionController@index')->name('session.queue');
    Route::get('/categories/get', 'Session\SessionController@getCreativeCategories')->name('session.categories');
    Route::get('/durations/get', 'Session\SessionController@getDurations')->name('session.durations');
    Route::get('/session/avatars', 'Session\SessionController@getRandomAvatars')->name('session.avatars');
    Route::get('/session/queue/pop', 'Session\SessionController@getQueuePop')->name('session.queue.pop');
    Route::post('/session/queue/ping', 'Session\SessionController@pingQueue')->name('session.queue.ping');
    Route::get('/session/queue/leave', 'Session\SessionController@leaveQueue')->name('session.queue.leave');
});

Route::get('/{user}', 'User\ProfileController@index')->name('profile');

// friendship
Route::post('/requestFriendship', 'User\FriendshipController@requestFriendship')->name('friendship.request');
Route::post('/revokeFriendship', 'User\FriendshipController@revokeFriendship')->name('friendship.revoke');
Route::post('/isFriends', 'User\FriendshipController@isFriend')->name('friendship.check');
Route::post('/acceptFriend', 'User\FriendshipController@acceptFriend')->name('friendship.accept');
Route::post('/denyFriend', 'User\FriendshipController@denyFriend')->name('friendship.deny');
Route::post('/isRequestPending', 'User\FriendshipController@isRequestPending')->name('friendship.pending');

// Notifications
Route::get('/notification/get', 'User\NotificationController@getNotifications')->name('notification.get');
Route::post('/notification/read', 'User\NotificationController@readNotification')->name('notification.read');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
